import cors from "cors";
import express from "express";
import mongoose from "mongoose";
import visitorRouter from "./routes/visitors.js";
import dotenv from "dotenv";
dotenv.config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;

if (uri) {
  mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true });
  const connection = mongoose.connection;
  connection.once("open", () => {
    console.log("MongoDB database connection established succesfully");
  });

  app.use(visitorRouter);

  app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
  });
} else {
  console.error("Cannot connect to mongo atlas uri");
}
