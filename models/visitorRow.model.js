import mongoose from "mongoose";

export const IRRADIANT = "IRRADIANT_CORE"
export const SEKVENSOFT = "SEKVENSOFT"

const Schema = mongoose.Schema;

const visitorRowSchema = new Schema({
  ip: { type: String, required: true },
  count: { type: Number, required: true },
  site: {
    type: String,
    enum: [IRRADIANT, SEKVENSOFT],
    required: true,
  },
});

const VisitorRow = mongoose.model("VisitorRow", visitorRowSchema);

export default VisitorRow;
