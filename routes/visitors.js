import express from "express";
import VisitorRow from "../models/visitorRow.model.js";
import { IRRADIANT, SEKVENSOFT } from "../models/visitorRow.model.js";

const visitorRouter = express.Router();

visitorRouter.route("/add").post(async (req, res) => {
  const ipExists = await VisitorRow.find({
    ip: req.body.ip,
    site: req.body.site,
  }).exec();

  if (ipExists.length > 0) {
    VisitorRow.updateOne(
      { _id: ipExists[0]._id },
      { count: ipExists[0].count + 1 }
    )
      .then(() => res.status(200).json("Visitor updated"))
      .catch((err) => res.status(400).json("Error: " + err));
  } else {
    const newVisitorRow = new VisitorRow({
      ip: req.body.ip,
      count: 1,
      site: req.body.site,
    });
    newVisitorRow
      .save()
      .then(() => res.status(200).json("Visitor added!"))
      .catch((err) => res.status(400).json("Error: " + err));
  }
});

visitorRouter.route("/sekvensoft").get((req, res) => {
  VisitorRow.find({site: SEKVENSOFT})
    .then((vr) => {
      let totalcount = 0;
      vr.forEach((v) => (totalcount = totalcount + v.count));
      res.status(200).json({ total: totalcount, different: vr.length });
    })
    .catch((err) => res.status(400).json("Error: " + err));
});

visitorRouter.route("/irradiant").get((req, res) => {
  VisitorRow.find({ site: IRRADIANT })
    .then((vr) => {
      let totalcount = 0;
      vr.forEach((v) => (totalcount = totalcount + v.count));
      res.status(200).json({ total: totalcount, different: vr.length });
    })
    .catch((err) => res.status(400).json("Error: " + err));
});

export default visitorRouter;
